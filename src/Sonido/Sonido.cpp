#include "Sonido.h"

namespace Sonido {

	Sound beep = LoadSound("../res/audio/beep.ogg");
	Sound plop = LoadSound("../res/audio/plop.ogg");
	Sound peep = LoadSound("../res/audio/peeeeep.ogg");
	Sound menu_musica = LoadSound("../res/audio/mainmenumusic.mp3");
	Sound pong_musica = LoadSound("../res/audio/pongamemusic.mp3");

	

	
	void inicializarAudio(Sound beep, Sound plop, Sound peep, Sound menu_musica,Sound pong_musica) {
		InitAudioDevice();
		SetSoundVolume(beep, 0.5f);
		SetSoundVolume(plop, 0.5f);
		SetSoundVolume(peep, 0.5f);
		SetSoundVolume(menu_musica, 0.5f);
		SetSoundVolume(pong_musica, 0.5f);
	}
	void modificarVolumen(Sound beep, Sound plop, Sound peep, Sound menu_musica, Sound pong_musica, float auxvolumen[],short input) {
		SetSoundVolume(beep, auxvolumen[input]);
		SetSoundVolume(plop, auxvolumen[input]);
		SetSoundVolume(peep, auxvolumen[input]);
		SetSoundVolume(menu_musica, auxvolumen[input]);
		SetSoundVolume(pong_musica, auxvolumen[input]);
	}
}