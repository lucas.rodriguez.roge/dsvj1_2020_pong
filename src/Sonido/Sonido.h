#ifndef SONIDO_H
#define SONIDO_H



#include "raylib.h"

namespace Sonido {
    extern Sound beep;
    extern Sound plop;
    extern Sound peep;
    extern Sound menu_musica;
    extern Sound pong_musica;
    void inicializarAudio(Sound beep, Sound plop, Sound peep, Sound menu_musica,Sound pong_musica);
    void modificarVolumen(Sound beep, Sound plop, Sound peep, Sound menu_musica, Sound pong_musica, float auxvolumen[], short input);
    
}
#endif // !SONIDO_H


