#ifndef PELOTA_H
#define PELOTA_H


#include "raylib.h"

namespace Ball {
    struct Pelota {
        Vector2 circulo_pos;
        float radio;
        Color color;
    };
    extern Pelota pelota;
    void init(Pelota& pelota);
    void DibujarPelota(Pelota& pelota);
    void MoverPelota(Pelota& pelota, bool& golpeopared, bool golpeojugador, Sound plop, float& movimiento_pelota);
    bool SiPelotaSefue(Pelota pelota,const int screenWidth);
}
#endif // !PELOTA_H
