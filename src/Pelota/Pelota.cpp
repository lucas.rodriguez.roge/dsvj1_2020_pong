#include "Pelota.h"
namespace Ball {
	Pelota pelota;
    void init(Pelota& pelota) {
        pelota.circulo_pos.x = 395; //pos x inicial
        pelota.circulo_pos.y = 225; //pos y inicial
        pelota.radio = 10; //radio de la pelota
        pelota.color = LIGHTGRAY;
    }
    bool SiPelotaSefue(Pelota pelota,const int screenWidth) {
        if (pelota.circulo_pos.x > screenWidth || pelota.circulo_pos.x < 0) {
            return true;
        }
        else
        {
            return false;
        }

    }

    void MoverPelota(Pelota& pelota, bool& golpeopared, bool golpeojugador, Sound plop, float& movimiento_pelota) {
        bool sonido = false; //flag para reproducir el sonido en la colision 


        if (golpeopared == false) {
            pelota.circulo_pos.y -= movimiento_pelota;
        }

        if (golpeopared == true) {
            pelota.circulo_pos.y += movimiento_pelota;
        }

        if (golpeojugador == false) {
            pelota.circulo_pos.x += movimiento_pelota;
        }

        if (golpeojugador == true) {
            pelota.circulo_pos.x -= movimiento_pelota;
        }


        if (pelota.circulo_pos.y >= 450) {
            sonido = true;
            golpeopared = false;
        }

        if (pelota.circulo_pos.y <= 0) {
            sonido = true;
            golpeopared = true;
        }
        if (sonido == true) {
            PlaySound(plop);
            sonido = false;
        }

        movimiento_pelota += 0.005f;

    }

    void DibujarPelota(Pelota& pelota) {
        DrawCircleV(pelota.circulo_pos, pelota.radio, pelota.color);

    }
}