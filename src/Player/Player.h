#ifndef PLAYER_H
#define PLAYER_H


#include "raylib.h"


namespace Player {
	struct Jugador {
		Rectangle size;
		Color colorj;                // la declaracion del jugador en el header se basa en la interaccion del menu hacia el jugador
		bool ia;
	};

	extern Jugador jugador1;
	extern Jugador jugador2;
	void init(Jugador & jugador1,Jugador & jugador2);
	void dibujarJugador1(Jugador& j1);
	void dibujarJugador2(Jugador& j2);
	void CustomizarPaletas(Jugador& j1, Jugador& j2, Color numpad_color_paletaj1[], Color numpad_color_paletaj2[], int& color_j1, int& color_j2);
	void ModoDeJuego(Jugador& j1, Jugador& j2, Color numpad_color_paletaj1[], Color numpad_color_paletaj2[], int& modo_j1, int& modo_j2);
}
#endif // !PLAYER_H