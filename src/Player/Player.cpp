#include "Player.h"
namespace Player {

	Jugador jugador1;
	Jugador jugador2;

	void init(Jugador & jugador1, Jugador& jugador2) {
		jugador1.size.width = 10; //ancho del jugador
		jugador1.size.height = 50;
		jugador1.colorj = RED;
		jugador1.size.x = 0;
		jugador1.size.y = 200;
		jugador1.ia = false;

		jugador2.size.width = 10;
		jugador2.size.height = 50;
		jugador2.colorj = BLUE;
		jugador2.size.x = 790;
		jugador2.size.y = 200;
		jugador2.ia = false;
	}

	void dibujarJugador1(Jugador& jugador1) {

		DrawRectangleRec(jugador1.size, jugador1.colorj);

	}

	void dibujarJugador2(Jugador& jugador2) {

		DrawRectangleRec(jugador2.size, jugador2.colorj);

	}

	void ModoDeJuego(Jugador& jugador1, Jugador& jugador2, Color numpad_color_paletaj1[], Color numpad_color_paletaj2[], int& modo_j1, int& modo_j2) {

		const char* modos[2] = { "PLAYER", "IA" };


		if (IsKeyDown(KEY_D)) {
			numpad_color_paletaj1[0] = RED;
			numpad_color_paletaj1[1] = WHITE;
		}
		if (modo_j1 < 1) {
			if (IsKeyPressed(KEY_D)) {
				modo_j1 = 1;
			}
		}
		else modo_j1 = 1;

		if (IsKeyDown(KEY_A)) {
			numpad_color_paletaj1[0] = WHITE;
			numpad_color_paletaj1[1] = RED;
		}
		if (modo_j1 > 0) {
			if (IsKeyPressed(KEY_A)) {
				modo_j1 = 0;
			}
		}

		if (IsKeyDown(KEY_RIGHT)) {
			numpad_color_paletaj2[0] = RED;
			numpad_color_paletaj2[1] = WHITE;
		}
		if (modo_j2 < 1) {
			if (IsKeyPressed(KEY_RIGHT)) {
				modo_j2 = 1;
			}
		}

		if (IsKeyDown(KEY_LEFT)) {
			numpad_color_paletaj2[0] = WHITE;
			numpad_color_paletaj2[1] = RED;
		}
		if (modo_j2 > 0) {
			if (IsKeyPressed(KEY_LEFT)) {
				modo_j2 = 0;
			}
		}


		DrawText("D", jugador1.size.x + 200, jugador1.size.y - 70, 40, numpad_color_paletaj1[0]);
		DrawText("A", jugador1.size.x, jugador1.size.y - 70, 40, numpad_color_paletaj1[1]);
		DrawText(">", jugador2.size.x - 20, jugador2.size.y - 70, 40, numpad_color_paletaj2[0]);
		DrawText("<", jugador2.size.x - 200, jugador2.size.y - 70, 40, numpad_color_paletaj2[1]);
		DrawRectangleRec(jugador2.size, jugador2.colorj);
		DrawRectangleRec(jugador1.size, jugador1.colorj);

		DrawText(modos[modo_j1], jugador1.size.x + 40, jugador1.size.y - 60, 20, jugador1.colorj);
		DrawText(modos[modo_j2], jugador2.size.x - 175, jugador2.size.y - 60, 20, jugador2.colorj);
		if (modo_j1 == 1) {
			jugador1.ia = true;
		}
		else jugador1.ia = false;
		if (modo_j2 == 1) {
			jugador2.ia = true;
		}
		else jugador2.ia = false;
	}

	void CustomizarPaletas(Jugador& jugador1, Jugador& jugador2, Color numpad_color_paletaj1[], Color numpad_color_paletaj2[], int& color_j1, int& color_j2) {

		Color cant_colores[11]{ RED,BLUE,GREEN,BROWN,PINK,GOLD,VIOLET,LIME,ORANGE,YELLOW,WHITE };



		if (IsKeyDown(KEY_D)) {
			numpad_color_paletaj1[0] = RED;
			numpad_color_paletaj1[1] = WHITE;
		}
		if (color_j1 < 10) {
			if (IsKeyPressed(KEY_D)) {
				color_j1++;
			}
		}
		else color_j1 = 10;

		if (IsKeyDown(KEY_A)) {
			numpad_color_paletaj1[0] = WHITE;
			numpad_color_paletaj1[1] = RED;
		}

		if (color_j1 > 0) {
			if (IsKeyPressed(KEY_A)) {
				color_j1--;
			}
		}
		else color_j1 = 0;
		if (IsKeyDown(KEY_RIGHT)) {
			numpad_color_paletaj2[0] = RED;
			numpad_color_paletaj2[1] = WHITE;
		}
		if (color_j2 < 10) {
			if (IsKeyPressed(KEY_RIGHT)) {
				color_j2++;
			}
		}
		else color_j2 = 10;
		if (IsKeyDown(KEY_LEFT)) {
			numpad_color_paletaj2[0] = WHITE;
			numpad_color_paletaj2[1] = RED;
		}
		if (color_j2 > 0) {
			if (IsKeyPressed(KEY_LEFT)) {
				color_j2--;
			}
		}
		else color_j2 = 0;

		jugador1.colorj = cant_colores[color_j1];
		jugador2.colorj = cant_colores[color_j2];

		DrawText("D", jugador1.size.x + 20, jugador1.size.y - 40, 40, numpad_color_paletaj1[0]);
		DrawText("A", jugador1.size.x + 20, jugador1.size.y + 40, 40, numpad_color_paletaj1[1]);
		DrawText(">", jugador2.size.x - 20, jugador2.size.y - 40, 40, numpad_color_paletaj2[0]);
		DrawText("<", jugador2.size.x - 20, jugador2.size.y + 40, 40, numpad_color_paletaj2[1]);
		DrawRectangleRec(jugador2.size, jugador2.colorj);
		DrawRectangleRec(jugador1.size, jugador1.colorj);
	}

}