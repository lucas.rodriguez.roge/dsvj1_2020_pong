#include <iostream>
#include "raylib.h"
#include "MenuPrincipal.h"
#include "../PongGame/PongGame.h"
#include "../Sonido/Sonido.h"
#include <string>

using namespace std;

namespace MenuPrincipal {
	void OpcionesMenu(OPCIONES& opciones, short& input, bool& gameover, Color eleccion[], bool& salio, Player::Jugador& jugador1, Player::Jugador& jugador2, int& color_j1, int& color_j2, short & input_volumen);
	void MenuPrincipal();
	namespace MenuOpciones {
		void inputVolumen(Color numpad_color_paletaj2[], short& input);
		void MostrarControles(Color j1, Color j2);
		void OpcionesOpcionesMenu(OPCIONESOPCIONES& opciones_opciones, bool& salio_opciones, Color  eleccion_opciones[], short& input_opciones, short& input_volumen, Player::Jugador& jugador1, Player::Jugador& jugador2, int& color_j1, int& color_j2, int& modo_j1, int& modo_j2);
	}
}

	

namespace MenuPrincipal {
	namespace MenuOpciones {
		void inputVolumen(Color numpad_color_paletaj2[], short& input) {

			float auxvolumen[6]{ 0,0.05f,0.2f,0.3f,0.4f,0.5f };

			if (IsKeyDown(KEY_RIGHT)) {
				numpad_color_paletaj2[0] = RED;
				numpad_color_paletaj2[1] = WHITE;
			}
			if (input < 5) {
				if (IsKeyPressed(KEY_RIGHT)) {
					input++;
				}
			}

			else input = 5;

			if (IsKeyDown(KEY_LEFT)) {

				numpad_color_paletaj2[0] = WHITE;
				numpad_color_paletaj2[1] = RED;

			}
			if (input > 0) {

				if (IsKeyPressed(KEY_LEFT)) {
					input--;
				}

			}
			else input = 0;


			for (short i = 0;i < 5;i++) {
				if (input > i) {
					DrawRectangle(340 + (i * 30), 360, 10, 10, WHITE);
				}
			}

			DrawText("<", 400 - 100, 350, 40, numpad_color_paletaj2[1]);
			DrawText(">", 400 + 100, 350, 40, numpad_color_paletaj2[0]);
			Sonido::modificarVolumen(Sonido::beep, Sonido::plop, Sonido::peep, Sonido::menu_musica, Sonido::pong_musica, auxvolumen, input);
		}

		void MostrarControles(Color j1, Color j2) {
			DrawText("JUGADOR 1", 0, screenHeight - 60, 20, j1);
			DrawText("JUGADOR 2", 450, screenHeight - 60, 20, j2);
			DrawText("W: MOVER HACIA ARRIBA", 0, screenHeight - 40, 20, j1);
			DrawText("S: MOVER HACIA ABAJO", 0, screenHeight - 20, 20, j1);
			DrawText("FLECHA ARRIBA: MOVER HACIA ARRIBA", 450, screenHeight - 40, 20, j2);
			DrawText("FLECHA ABAJO: MOVER HACIA ABAJO", 450, screenHeight - 20, 20, j2);

		}

		void OpcionesOpcionesMenu(OPCIONESOPCIONES& opciones_opciones, bool& salio_opciones, Color  eleccion_opciones[], short& input_opciones, short& input_volumen, Player::Jugador& jugador1, Player::Jugador& jugador2, int& color_j1, int& color_j2, int& modo_j1, int& modo_j2) {
			BeginDrawing();

			ClearBackground(BLACK);

			DrawText("PONG", 325, 20, 70, LIGHTGRAY);


			opciones_opciones = (OPCIONESOPCIONES)input_opciones;

			DrawText("MODO", 300, 100, 40, eleccion_opciones[0]);
			DrawText("PALETAS", 300, 150, 40, eleccion_opciones[1]);
			DrawText("CONTROLES", 300, 200, 40, eleccion_opciones[2]);
			DrawText("VOLUMEN", 300, 250, 40, eleccion_opciones[3]);
			DrawText("VOLVER", 300, 300, 40, eleccion_opciones[4]);

			Color numpad_color_paletaj1[2]{ WHITE,WHITE };
			Color numpad_color_paletaj2[2]{ WHITE,WHITE };

			switch (opciones_opciones) {
			case OPCIONESOPCIONES::MODO:
				eleccion_opciones[input_opciones] = RED;
				Player::ModoDeJuego(Player::jugador1, Player::jugador2, numpad_color_paletaj1, numpad_color_paletaj2, modo_j1, modo_j2);
				if (IsKeyPressed(KEY_DOWN)) {

					input_opciones++;
					eleccion_opciones[input_opciones - 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;
				}
				break;
			case OPCIONESOPCIONES::PALETAS:

				eleccion_opciones[input_opciones] = RED;
				Player::CustomizarPaletas(Player::jugador1, Player::jugador2, numpad_color_paletaj1, numpad_color_paletaj2, color_j1, color_j2);
				if (IsKeyPressed(KEY_UP)) {

					input_opciones--;
					eleccion_opciones[input_opciones + 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;

				}
				if (IsKeyPressed(KEY_DOWN)) {

					input_opciones++;
					eleccion_opciones[input_opciones - 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;
				}

				break;

			case OPCIONESOPCIONES::CONTROLES:

				eleccion_opciones[input_opciones] = RED;

				MostrarControles(Player::jugador1.colorj, Player::jugador2.colorj);

				if (IsKeyPressed(KEY_DOWN)) {

					input_opciones++;
					eleccion_opciones[input_opciones - 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;
				}

				if (IsKeyPressed(KEY_UP)) {

					input_opciones--;
					eleccion_opciones[input_opciones + 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;

				}
				break;
			case OPCIONESOPCIONES::VOLUMEN:

				eleccion_opciones[input_opciones] = RED;
				inputVolumen(numpad_color_paletaj2, input_volumen);

				if (IsKeyPressed(KEY_DOWN)) {

					input_opciones++;
					eleccion_opciones[input_opciones - 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;
				}

				if (IsKeyPressed(KEY_UP)) {

					input_opciones--;
					eleccion_opciones[input_opciones + 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;

				}
				break;
			case OPCIONESOPCIONES::VOLVER:

				eleccion_opciones[input_opciones] = RED;
				if (IsKeyPressed(KEY_ENTER)) {
					salio_opciones = true;

				}
				if (IsKeyPressed(KEY_UP)) {

					input_opciones--;
					eleccion_opciones[input_opciones + 1] = WHITE;
					opciones_opciones = (OPCIONESOPCIONES)input_opciones;

				}

				break;
			}

			EndDrawing();
		}
	}
	

	void OpcionesMenu(OPCIONES& opciones, short& input, bool& gameover, Color eleccion[], bool& salio, Player::Jugador& jugador1, Player::Jugador& jugador2, int& color_j1, int& color_j2, short & input_volumen) {

		BeginDrawing();
		ClearBackground(BLACK);
		DrawText("PONG", 325, 20, 70, LIGHTGRAY);

		DrawText("Made by - Gentlemen Doggo v0.3", 0, 430, 15, LIGHTGRAY);

		DrawText("JUGAR", 350, 100, 40, eleccion[0]);
		DrawText("OPCIONES", 350, 200, 40, eleccion[1]);
		DrawText("SALIR", 350, 300, 40, eleccion[2]);

		
		short input_opciones = 0;
		bool salio_opciones = false;
		OPCIONESOPCIONES opciones_opciones;
		Color eleccion_opciones[5]{ WHITE,WHITE,WHITE,WHITE,WHITE };
		int modo_j1 = 0;
		int modo_j2 = 0;
		switch (opciones) {

		case OPCIONES::JUGAR:

			eleccion[input] = RED;

			if (IsKeyPressed(KEY_DOWN)) {

				input++;
				eleccion[input - 1] = WHITE;
				opciones = (OPCIONES)input;
			}

			if (IsKeyPressed(KEY_ENTER)) {

				StopSound(Sonido::menu_musica);
				gameplay::Juego(gameover, Player::jugador1, Player::jugador2);
				gameover = false;

			}

			break;
		case OPCIONES::OPCIONES:

			eleccion[input] = RED;

			if (IsKeyPressed(KEY_UP)) {

				input--;
				eleccion[input + 1] = WHITE;
				opciones = (OPCIONES)input;

			}
			if (IsKeyPressed(KEY_ENTER)) {
				while (salio_opciones == false) {
					MenuOpciones::OpcionesOpcionesMenu(opciones_opciones, salio_opciones, eleccion_opciones, input_opciones, input_volumen, Player::jugador1, Player::jugador2, color_j1, color_j2, modo_j1, modo_j2);
				}

			}

			if (IsKeyPressed(KEY_DOWN)) {

				input++;
				eleccion[input - 1] = WHITE;
				opciones = (OPCIONES)input;
			}
			break;
		case OPCIONES::SALIR:
			eleccion[input] = RED;

			if (IsKeyPressed(KEY_UP)) {
				input--;
				eleccion[input + 1] = WHITE;
				opciones = (OPCIONES)input;
			}

			if (IsKeyPressed(KEY_ENTER)) {
				salio = true;
			}
			break;

		}

		EndDrawing();
	}

	void MenuPrincipal() {


		InitWindow(screenWidth, screenHeight, "PONG - MADE BY GENTLEMEN DOGGO <3- Main Menu v0.3");

		Sonido::inicializarAudio(Sonido::beep, Sonido::plop, Sonido::peep, Sonido::menu_musica, Sonido::pong_musica);

		PlaySound(Sonido::menu_musica);

		SetTargetFPS(60);   // Set our game to run at 60 frames-per-second

		bool salio = false;

		while (!WindowShouldClose() && salio == false) {   // Detect window close button or ESC key

			BeginDrawing();

			ClearBackground(BLACK);


			Color eleccion[3]{ WHITE,WHITE,WHITE };

			short input = 0;

			Player::init(Player::jugador1, Player::jugador2);
			OPCIONES opciones;
			opciones = (OPCIONES)input;

			int color_j1 = 0;
			int color_j2 = 1;

			short input_volumen = 5;

			bool gameover = false;

			while (salio == false) {

				OpcionesMenu(opciones, input, gameover, eleccion, salio, Player::jugador1, Player::jugador2, color_j1, color_j2,input_volumen);

			}
			EndDrawing();
			StopSound(Sonido::menu_musica);
			UnloadSound(Sonido::menu_musica);


		}
	}
}
