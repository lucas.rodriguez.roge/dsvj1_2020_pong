
#include <iostream>
#include "raylib.h"
#include "PongGame.h"
#include "../Sonido/Sonido.h"
#include "../Item/Item.h"
#include "../Pelota/Pelota.h"


using namespace Sonido;
using namespace std;


namespace gameplay {
    struct Score {
        short j1;
        short j2;
    };
    //declaracion funciones
    
    void init(Ball::Pelota& pelota, item::Item& power_up_crecimiento, Score& score, bool& spawnpowerup, bool& golpeopared, bool& golpeojugador, bool& inicio, bool& pausa, float& movimiento_pelota, MENUPAUSA & menupausa, Color& continuar, Color& salir);
    void update(Ball::Pelota& pelota, item::Item& power_up_crecimiento, Score& score, bool& spawnpowerup, bool& golpeopared, bool& golpeojugador, bool& inicio, bool& pausa, float& movimiento_pelota, Player::Jugador& jugador1, Player::Jugador& jugador2, bool& gameover, MENUPAUSA& menupausa, Color& continuar, Color& salir);
    void draw(Score score, bool inicio, Player::Jugador jugador1, Player::Jugador jugador2, bool spawnpowerup, item::Item power_up_crecimiento);
    void PausaMenu(bool& pausa, bool& inicio, MENUPAUSA& menupausa, Color& continuar, Color& salir, bool& gameover);
    void MostrarScore(Score score);
    void ControlColisiones(Ball::Pelota & pelota, bool& golpeojugador, Player::Jugador& jugador1, Player::Jugador& jugador2, item::Item& power_up_crecimiento, bool& spawnpowerup);
    void AnotarYReiniciar(bool& inicio, Ball::Pelota& pelota, Score& score, Player::Jugador & jugador1, Player::Jugador & jugador2, float& movimiento_pelota);
    void MostrarTextoInicio(bool inicio);
    void ControlesJugador1(Player::Jugador& j1, Ball::Pelota pelota);
    void ControlesJugador2(Player::Jugador& j2, Ball::Pelota pelota);
    void FinalizarElJuego(Score score, bool& gameover, Player::Jugador& jugador1, Player::Jugador& jugador2);
    void Juego(bool& gameover, Player::Jugador& jugador1, Player::Jugador& jugador2);

    void init(Ball::Pelota & pelota, item::Item & power_up_crecimiento, Score & score,bool & spawnpowerup,bool &golpeopared,bool & golpeojugador,bool & inicio,bool & pausa,float & movimiento_pelota,MENUPAUSA & menupausa, Color & continuar, Color & salir) {
        score.j1 = 0;
        score.j2 = 0;

        menupausa = MENUPAUSA::CONTINUAR;

        continuar = RED;
        salir = WHITE;

        Ball::init(Ball::pelota);
        item::init(item::power_up_crecimiento);
        spawnpowerup = false;

        golpeopared = false; //flag para saber si golpeo screenWidth y screen Height
        golpeojugador = false; //flag para saber si golpeo un jugador

        inicio = false; //flag para el restart del juego cada vez que un jugador anota un punto
        pausa = false;

        movimiento_pelota = 3;
        SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
      //--------------------------------------------------------------------------------------


    }

    void update(Ball::Pelota& pelota, item::Item& power_up_crecimiento, Score& score, bool& spawnpowerup, bool& golpeopared, bool& golpeojugador, bool& inicio, bool& pausa, float& movimiento_pelota,Player::Jugador & jugador1,Player::Jugador & jugador2,bool &gameover, MENUPAUSA & menupausa, Color& continuar, Color& salir) {

        if (inicio == true && pausa == false) {




            ControlesJugador1(jugador1, Ball::pelota); //movimiento jugador 1
            ControlesJugador2(jugador2, Ball::pelota); //movimiento jugador 2

            Ball::MoverPelota(Ball::pelota, golpeopared, golpeojugador, plop, movimiento_pelota); //mueve la pelota dentro del area jugable

        }

        if (IsKeyPressed(KEY_ENTER)) {
            inicio = true;
        }

        ControlColisiones(Ball::pelota, golpeojugador, Player::jugador1, jugador2, item::power_up_crecimiento, spawnpowerup);

        AnotarYReiniciar(inicio, Ball::pelota, score, Player::jugador1, Player::jugador2, movimiento_pelota);

        PausaMenu(pausa, inicio, menupausa, continuar, salir, gameover);

        if ((short)GetTime() % 10 == 0 && inicio == true && spawnpowerup == false) {
            spawnpowerup = true;

            PowerUpAgrandarPaletaSpawn(power_up_crecimiento,screenWidth,screenHeight);

        }
        FinalizarElJuego(score, gameover, jugador1, jugador2);
    }

    void draw(Score score, bool inicio,Player::Jugador jugador1, Player::Jugador jugador2, bool spawnpowerup,item::Item power_up_crecimiento) {
        BeginDrawing();

        MostrarScore(score);

        MostrarTextoInicio(inicio);

        
        Player::dibujarJugador1(jugador1);
        Player::dibujarJugador2(jugador2);
        item::SpawnPowerUp(item::power_up_crecimiento, spawnpowerup);
        Ball::DibujarPelota(Ball::pelota);

        ClearBackground(BLACK);

        EndDrawing();
    }

    void PausaMenu(bool & pausa,bool & inicio,MENUPAUSA & menupausa,Color & continuar,Color & salir, bool & gameover) {
        if (pausa == true) {
            DrawText("CONTINUAR", 350, 150, 40, continuar);
            DrawText("SALIR", 350, 250, 40, salir);
            switch (menupausa) {
            case MENUPAUSA::CONTINUAR:
                continuar = RED;
                salir = WHITE;
                if (IsKeyPressed(KEY_ENTER)) {
                    pausa = false;

                }
                if (IsKeyPressed(KEY_DOWN)) {
                    menupausa = MENUPAUSA::SALIR;
                }
                break;
            case MENUPAUSA::SALIR:
                salir = RED;
                continuar = WHITE;
                if (IsKeyPressed(KEY_ENTER)) {
                    gameover = true;
                }
                if (IsKeyPressed(KEY_UP)) {
                    menupausa = MENUPAUSA::CONTINUAR;
                }
                break;
            }
        }
        if (inicio == true) {
            if (IsKeyPressed(KEY_P)) {

                pausa = true;
            }
        }
    }

    void ControlColisiones(Ball::Pelota & pelota, bool& golpeojugador, Player::Jugador& jugador1, Player::Jugador& jugador2, item::Item& power_up_crecimiento, bool& spawnpowerup) {

        if (CheckCollisionCircleRec(Ball::pelota.circulo_pos, Ball::pelota.radio, Player::jugador1.size)) {
            PlaySound(beep);
            golpeojugador = false;
            Ball::pelota.color = Player::jugador1.colorj;
        }
        else if (CheckCollisionCircleRec(Ball::pelota.circulo_pos, Ball::pelota.radio, Player::jugador2.size)) {
            PlaySound(beep);
            golpeojugador = true;
            Ball::pelota.color = Player::jugador2.colorj;
        }
        else if (CheckCollisionCircles(Ball::pelota.circulo_pos, Ball::pelota.radio, item::power_up_crecimiento.item_pos, item::power_up_crecimiento.radio)) {
            spawnpowerup = false;
            if (golpeojugador == true) {

                Player::jugador2.size.height += 10;

            }
            if (golpeojugador == false) {

                Player::jugador1.size.height += 10;

            }

        }


    }

    void MostrarScore(Score score) {
        DrawText(FormatText("%d", score.j1), 150, 20, 20, LIGHTGRAY);
        DrawText("/10", 165, 20, 20, LIGHTGRAY);
        DrawText(FormatText("%d", score.j2), screenWidth - 150, 20, 20, LIGHTGRAY);
        DrawText("/10", screenWidth - 135, 20, 20, LIGHTGRAY);
    }

    void FinalizarElJuego(Score score, bool& gameover, Player::Jugador& jugador1, Player::Jugador& jugador2) {



        if (score.j1 > 9) {

            gameover = true;

            Player::jugador1.size.width = 10; //ancho del jugador
            Player::jugador1.size.height = 50;
            Player::jugador1.colorj = RED;
            Player::jugador1.size.x = 0;
            Player::jugador1.size.y = 200;
            Player::jugador1.ia = false;

            Player::jugador2.size.width = 10;
            Player::jugador2.size.height = 50;
            Player::jugador2.colorj = BLUE;
            Player::jugador2.size.x = 790;
            Player::jugador2.size.y = 200;
            jugador2.ia = false;
        }

        if (score.j2 > 9) {

            gameover = true;

            Player::jugador1.size.width = 10; //ancho del jugador
            Player::jugador1.size.height = 50;
            Player::jugador1.colorj = RED;
            Player::jugador1.size.x = 0;
            Player::jugador1.size.y = 200;
            Player::jugador1.ia = false;

            Player::jugador2.size.width = 10;
            Player::jugador2.size.height = 50;
            Player::jugador2.colorj = BLUE;
            Player::jugador2.size.x = 790;
            Player::jugador2.size.y = 200;
            Player::jugador2.ia = false;

        }


    }

    void AnotarYReiniciar(bool& inicio, Ball::Pelota& pelota, Score& score, Player::Jugador & jugador1,Player::Jugador & jugador2, float& movimiento_pelota) {

        short xinicio = 395;
        short yinicio = 225;

        if (Ball::SiPelotaSefue(Ball::pelota,screenWidth) == true) {
            PlaySound(peep);
            if (Ball::pelota.circulo_pos.x > screenWidth) {
                score.j1++;
            }
            else {
                score.j2++;
            }
            inicio = false;
            movimiento_pelota = 3;
            Ball::pelota.circulo_pos.x = xinicio;
            Ball::pelota.circulo_pos.y = yinicio;
            Player::jugador1.size.y = 200;
            Player::jugador2.size.y = 200;
            Player::jugador1.size.height = 50;
            Player::jugador2.size.height = 50;

        }
    }

    void MostrarTextoInicio(bool inicio) {
        if (inicio == false) {
            DrawText("PRESIONE ENTER PARA COMENZAR A JUGAR", 200, 200, 20, LIGHTGRAY);
        }
    }

    void ControlesJugador1(Player::Jugador& jugador1, Ball::Pelota pelota) {
        if (Player::jugador1.size.y >= 400) {
            Player::jugador1.size.y = 400;
        }
        if (Player::jugador1.size.y <= 0) {
            Player::jugador1.size.y = 0;
        }
        if (Player::jugador1.ia == false) {
            if (IsKeyPressed(KEY_W))
                Player::jugador1.size.y -= movementy;
            if (IsKeyPressed(KEY_S))
                Player::jugador1.size.y += movementy;
        }
        if (Player::jugador1.ia == true) {
            while (Ball::pelota.circulo_pos.y > Player::jugador1.size.y) {
                Player::jugador1.size.y += movementy;
            }
            while (Ball::pelota.circulo_pos.y < Player::jugador1.size.y) {
                Player::jugador1.size.y -= movementy;
            }
        }

    }

    void ControlesJugador2(Player::Jugador& jugador2, Ball::Pelota pelota) {
        if (Player::jugador2.size.y >= 400) {
            Player::jugador2.size.y = 400;
        }
        if (Player::jugador2.size.y <= 0) {
            Player::jugador2.size.y = 0;
        }
        if (Player::jugador2.ia == false) {
            if (IsKeyPressed(KEY_UP))
                Player::jugador2.size.y -= movementy;
            if (IsKeyPressed(KEY_DOWN))
                Player::jugador2.size.y += movementy;
        }
        if (Player::jugador2.ia == true) {
            while (Ball::pelota.circulo_pos.y > Player::jugador2.size.y) {

                Player::jugador2.size.y += movementy;

            }
            while (Ball::pelota.circulo_pos.y < Player::jugador2.size.y) {

                Player::jugador2.size.y -= movementy;

            }
        }
    }

    void Juego(bool& gameover, Player::Jugador& jugador1, Player::Jugador& jugador2){

        // Initialization
        //--------------------------------------------------------------------------------------
        MENUPAUSA menupausa;
        Score score;
        Color continuar,salir;
        bool spawnpowerup, golpeopared, golpeojugador, inicio, pausa;
        float movimiento_pelota;
        init(Ball::pelota, item::power_up_crecimiento, score, spawnpowerup, golpeopared, golpeojugador, inicio, pausa, movimiento_pelota,menupausa,continuar,salir);
        PlaySound(Sonido::pong_musica);
        // Main game loop
        while (!gameover)    
        {

            update(Ball::pelota, item::power_up_crecimiento, score, spawnpowerup, golpeopared, golpeojugador, inicio, pausa, movimiento_pelota, Player::jugador1, Player::jugador2,gameover, menupausa,continuar,salir);

            draw(score, inicio, Player::jugador1, Player::jugador2,spawnpowerup,item::power_up_crecimiento);

        }
        StopSound(Sonido::pong_musica);

    }

}



