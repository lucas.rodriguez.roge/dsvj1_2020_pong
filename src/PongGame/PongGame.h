#ifndef PONGGAME_H
#define PONGGAME_H



#include "raylib.h"
#include "../Player/Player.h"


//screenWidth: ancho de la ventana
const int screenWidth = 800;

//screenHeight: largo de la ventana
const int screenHeight = 450;

namespace gameplay {
	//opciones pausa
	enum class MENUPAUSA { CONTINUAR, SALIR };
	// movementy: movimiento jugadores 1 y 2
	const short movementy = 20;
	void Juego(bool& gameover, Player::Jugador& jugador1, Player::Jugador& jugador2);
}
#endif // !PONGGAME_H