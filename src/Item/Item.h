#ifndef ITEM_H
#define ITEM_H


#include "raylib.h"


namespace item {
    struct Item {
        Vector2 item_pos;
        float radio;
        Color color;
    };
   extern Item power_up_crecimiento;
   void init(Item& power_up_crecimiento);
   void PowerUpAgrandarPaletaSpawn(Item& power_up_crecimiento, const int screenWidth, const int screenHeight);
   void SpawnPowerUp(Item power_up_crecimiento, bool spawnpowerupflag);
}
#endif // !ITEM_H
