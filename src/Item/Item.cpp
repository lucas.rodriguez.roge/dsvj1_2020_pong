#include "Item.h"
namespace item {
	Item power_up_crecimiento;
    void init(Item& power_up_crecimiento) {
        power_up_crecimiento.color = RED;
        power_up_crecimiento.item_pos.x = 5;
        power_up_crecimiento.item_pos.y = 5;
        power_up_crecimiento.radio = 5;

    }
    void PowerUpAgrandarPaletaSpawn(Item& power_up_crecimiento,const int screenWidth,const int screenHeight) {

        power_up_crecimiento.item_pos.x = GetRandomValue(20, screenWidth - 20);
        power_up_crecimiento.item_pos.y = GetRandomValue(20, screenHeight - 20);
        power_up_crecimiento.radio = 20;

        power_up_crecimiento.color = RAYWHITE;


    }
    void SpawnPowerUp(Item power_up_crecimiento, bool spawnpowerupflag) {
        if (spawnpowerupflag == true) {
            DrawCircleV(power_up_crecimiento.item_pos, power_up_crecimiento.radio, power_up_crecimiento.color);
        }
    }
}